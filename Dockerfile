# Learn more about the Docker images here:
# https://gitlab.com/paddy-hack/devuan/blob/master/README.md


# Things won't work with ruby 2.5 on beowulf, someone should look into that.
FROM registry.gitlab.com/paddy-hack/devuan:ascii

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    build-essential ruby ruby-dev zlib1g-dev libcurl3 pandoc git

RUN gem install rake middleman && gem install bundler

# Pre-install requisites to allow for quick subsequent webpage builds
RUN mkdir -p /devuan-www/src
COPY Gemfile /devuan-www/Gemfile
COPY Gemfile.lock /devuan-www/Gemfile.lock
RUN cd /devuan-www && bundle install --path /devuan-www/vendor/bundle

# If /devuan-www/src/Gemfile does not exist, following repo will be cloned
# You could e.g. mount /devuan-www/src as a docker volume
ENV DEVUAN_WWW_REPO   https://git.devuan.org/devuan-editors/devuan-www.git
ENV DEVUAN_WWW_BRANCH master

# Built site will be placed in a `public` subdir
# A build log will be placed in `build.log`
ENV DEVUAN_DIR_OUT /devuan-www/out

COPY docker_init.sh  /docker_init.sh
CMD ./docker_init.sh
