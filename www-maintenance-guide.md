# WWW Maintenance Guide

This document provides an introduction to the tasks required to
update `www.devuan.org` for new and point releases as well as a few
non-release-specific tasks.  Every page on the site is listed with
a description of the content and whether it is static or requiring
attention for releases or other maintenance.

All changes to WWW are first made at:

https://git.devuan.org/devuan/www.devuan.org/src/branch/new-beta

When vetted, the changes are published to www.devuan.org by touching
this file:

https://git.devuan.org/devuan/www.devuan.org/src/branch/new-beta/.PUBLISH

## Devuan WWW Tasks at a Glance

### `## www.devuan.org/source/`

Note that the following three pages and the following error page are
the only ones that have the "free-software banner" at the top of the
page.

1. `index.html`

   Provides basic information and navigation options.

   * Requires updating with a new release.

2. `get-devuan.html`

   Describes available isos and lists iso mirror options as well as
   Torrent and Magnet links.

   * Requires updating with a new release as well as adding new
     mirrors or removing those that are broken as necessary.

3. `sitemap.xml`

   Assists search engine indexing.

   * `sitemap.xml` is generated automatically by
     `/etc/cron.d/devuan-www` on the server(s) and runs every minute
     to a) check out `beta.devuan.org` and b) to check the `.PUBLISH`
     file; for changes.

#### `### error/404.html`
Standard 404.html error page
* Static content

#### `### www.devuan.org/os/`

##### `#### announce/`
Release and other important announcements are archived here.
* Update as needed
 
###### `#### report/`
Devuan financial reports and related graphics
* maintained by Dyne

##### `#### documentation/`

1. design-guide/devuan-icons.html

   List of Devuan icons.
   * Static content

2. `install-guides/`

   Includes annotated screenshots of both an installer and a live iso
   installation for every Devuan release as well as instructions for
   specific installation issues.

   * Updated for each new release and to correct errors and make other
     changes as needed.

#### `### www.devuan.org/os/`

1. `community.html`

   Lists community resources like IRC, Dev1Galaxy forum, mail lists
   etc.
   * Content occasionally added or removed as needed.

2. `contact.html`

   How to contact Devuan.
   * Static content

3. `development.html`

   Assorted links to and information about various aspects of Devuan
   development.
   * Occasional revisions may be needed.

4. `devuan-distros.html`

   List of approved Devuan derivatives.
   * Around the time of a new release, check which ones have upgraded
     and edit accordingly.

5. `distro-kit.html`

   Presentation of Devuan development tools.
   * Static content

6. `donate.html`

   Where and how to donate to the Devuan project.
   * Dyne admins this page.

7. `explore.html`

   Index of every user-facing page on www as well as a few off-site
   informational links.
   * Mostly static except for release or other announcements.

8. `filenaming.html`

   Describes Devuan's filenaming conventions.
   * Static content

9. `free-software.html`

   In house essay about free software.
   * Static content

10. `gitlab-issues.html`

    Archived insight to early Devuan development.
    * Static content

11. `index.html`

    Summary of current release specifications.
    * Requires update with each release and possibly at other times.

12. `init-freedom.html`

    Lists other distributions that offer various init options.
    * Should check that this list is up-to-date with each new release.

13. `install.html`

    Basic installation information and links to release specific
    Devuan documentation.
    * Requires update with each new release.

14. `keyring.html`

    Detailed keyring information.
    * Requires update with each new release and possibly at other
      times.

15. `packages.html`

    Repository tips, cautions and release specific setup information.
    * Requires update with each new release.

16. `releases.html`

    Lists Devuan release codenames and describes the role of suites
    and archives.
    * Requires update with each new release.

17. `source-code.html`

    Discussion of source code, licensing, firmware and links to
    https://www.devuan.org source code at https://git.devuan.org.
    * Static content

18. `team.html`

    List of users who have contributed to Devuan over the years.
    * Add new devs to the list as appropriate, usually with
      a new release.  Description should fit on one line and
      be playful/memorable.

#### `### ui/css/`

This directory contains the CSS for:
1. https://www.devuan.org
2. https://bugs.devuan.org
3. https://popcon.devuan.org  
   as well as the complete source for:
4. https://pkginfo.devuan.org.

##### `#### debbugs/`

The strange naming of these files was to indicate that the files are
nearly identical.  It seemed like a good idea at the time!

###### `##### buggers.css`
CSS for https://popcon.devuan.org
* Will require updating if theme changes

###### `##### bugs.css`
CSS for https://bugs.devuan.org
* Will require updating if theme changes

##### `#### fonts/`
* Static content

##### `#### pkginfo/`
Source and css for https://pkginfo.devuan.org
* Will require updating if theme changes

##### `#### devuan.css`
CSS for https://www.devuan.org
* Will require updating if theme changes

#### `### ui/img/`
* Will require updating if theme changes

## Other Maintenance Tasks

* In addition to updating visible website content for a new release,
  information in the header also has to be updated.

* Change Copyright date on all pages when site is updated for the
  first time in the New Year.
 
* Post new announcements as provided by original author(s).
  Any alterations must be approved by the original author(s).

* All additions should be consistent with current website visual
  formatting i.e. position, spacing, styling, color etc.

## Additional Resources

Xenguy's notes will help guide you through the labyrinth:

https://git.devuan.org/Xenguy/Xenguy_website-notes/raw/branch/master/README.md

-------------------------------

Special thanks to bandali for proofing the text and translating it to
markdown and also to Xenguy for contributing detailed technical notes
and good advice, as always. ~ golinux
