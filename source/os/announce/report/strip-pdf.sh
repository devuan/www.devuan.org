#!/bin/sh

tmp=`mktemp`
rm $tmp
exiftool -all:all="" $1 -o $tmp
qpdf --linearize $tmp $1
rm -f $tmp
exiftool $1
exiftool -PDF-update:all= $1
