#!/bin/bash

W=1024
H=768
# Donations

cat <<EOF | gnuplot
reset
clear

set terminal png size $W,$H
set output "donations.png"
set key top left outside horizontal autotitle columnhead
set title "Donations to Devuan"

set key autotitle columnheader
set xlabel "Years"

set ylabel "Money (EUR)"
set ytics out nomirror

set linetype 1 lc rgb 'black'
set linetype 2 lc rgb '#555555'
set linetype 3 lc rgb '#999999'

set style data histogram
set style histogram rowstacked
set style fill pattern

set boxwidth 0.9 absolute
set key samplen 2.5 spacing 0.85

plot "donations.tsv" using 2:xtic(1), "" using 3, "" using 4
EOF

# Expenses

cat <<EOF | gnuplot
reset
clear

set terminal png size $W,$H
set output "expenses.png"
set key top left outside horizontal autotitle columnhead
set title "Expenses for Devuan"

set key autotitle columnheader
set xlabel "Years"

set ylabel "Money (EUR)"
set ytics out nomirror

set linetype 1 lc rgb 'black'
set linetype 2 lc rgb '#555555'
set linetype 3 lc rgb '#999999'

set style data histogram
set style histogram rowstacked
set style fill pattern

set boxwidth 0.9 absolute
set key samplen 2.5 spacing 0.85

plot "expenses.tsv" using 2:xtic(1)
EOF
